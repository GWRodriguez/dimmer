const int pinPot = A0;
int pot = 0;

const int pinLED = 9;
int led = 0;



void setup() {
  pinMode(pinLED, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  pot = analogRead(pinPot);
  Serial.print("Pot value: ");
  Serial.print(pot);

  led = map(pot, 1023, 0, 0, 255);
  Serial.print(" LED: ");
  Serial.println(led);

  analogWrite(pinLED, led);

  delay(10);
}
